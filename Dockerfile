FROM alpine

ENV DOCKERVERSION=18.06.1-ce
RUN apk --no-cache add curl bash dialog

RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKERVERSION}.tgz \
    && tar xzvf docker-${DOCKERVERSION}.tgz --strip 1 \
    -C /usr/local/bin docker/docker \
    && rm docker-${DOCKERVERSION}.tgz

ADD ./bin /bin

CMD ["/bin/bash"]