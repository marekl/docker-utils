ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

FILES = `ls bin/`
AUTOCOMPLETE = `ls autocomplete`
DOCKER = `which docker`

build:
	@echo "No need to build. Just install it!"

install: bin/* autocomplete/*
	@install -d $(DESTDIR)$(PREFIX)/bin/
	@install -m 755 bin/* $(DESTDIR)$(PREFIX)/bin/
	@install -d /etc/bash_completion.d/
	@install -m 644 autocomplete/* /etc/bash_completion.d/

uninstall:
	@for f in $(FILES); do \
		rm $(DESTDIR)$(PREFIX)/bin/$$f ; \
	done

publish:
	$(DOCKER) build dvim -t reg.marekl.cz/dvim && $(DOCKER) push reg.marekl.cz/dvim
	$(DOCKER) build . -t reg.marekl.cz/docker-utils && $(DOCKER) push reg.marekl.cz/docker-utils